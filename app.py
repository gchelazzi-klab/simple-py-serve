import http.server
import socket
import socketserver

PORT = 8080

Handler = http.server.SimpleHTTPRequestHandler

# get the local ip
local_ip = socket.gethostbyname(socket.gethostname())
# create a file with a huge vulnerability in it
with open("./local_ip", 'a') as out:
    out.write(local_ip)
    out.close()
# serve it
with socketserver.TCPServer(("", PORT), Handler) as httpd:
    print("serving at port", PORT)
    httpd.serve_forever()
